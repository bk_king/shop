<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 17:36
 */

namespace Bkrol\GameShop\Controller;


use Bkrol\GameShop\AdminPanel\AccountRepository;
use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\Request\Request;
use Bkrol\GameShop\Routing\Routing;
use Bkrol\GameShop\Session\Session;
use Bkrol\GameShop\ShopPanel\Category\GameCategoryRepository;
use Bkrol\GameShop\ShopPanel\Games\GamesRepository;
use Bkrol\GameShop\ShopPanel\Orders\OrdersRepository;
use Bkrol\GameShop\ShopPanel\Users\UsersRepository;
use Bkrol\GameShop\Validators\FormValidator;
use Bkrol\GameShop\Validators\ImageValidator;
use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

class Controller
{

    private $session;
    private $request;
    private $templates;
    private $account;
    private $gameCategory;
    private $formValidator;
    private $game;
    private $imageValidator;
    private $users;
    private $orders;

    public function __construct(
        Session $session,
        Request $request,
        Twig_Environment $twig,
        AccountRepository $account

    )
    {
        $this->session = $session;
        $this->request = $request;
        $this->templates = $twig;
        $this->account = $account;
        $this->gameCategory = new GameCategoryRepository();
        $this->formValidator = new FormValidator();
        $this->game = new GamesRepository();
        $this->imageValidator = new ImageValidator();
        $this->users = new UsersRepository();
        $this->orders = new OrdersRepository();


    }

    /**
     *
     */
    public function run()
    {
        if (($action = $this->request->getParamFormGet('action', false)) === false) {
            $action = Routing::MAIN_PAGE;
        }
        switch ($action) {
            case Routing::ADMIN_INDEX:
                $logged = $this->session->get('logged', false);
                $login = $this->session->get('login', false);
                if ($logged === 'loggedIn' && $login !== false) {
                    $this->redirect('adminMain');
                }
                $params = [];
                if (($login = $this->request->getParamsFormPost('login', false)) !== false &&
                    ($password = $this->request->getParamsFormPost('password', false)) !== false
                ) {
                    if (($account = $this->account->getUserByLoginAndPassword($login, $password))) {
                        $this->session->put('login', $account->getLogin());
                        $this->session->put('id', $account->getId());
                        $this->session->put('logged', 'loggedIn');
                        $this->redirect(Routing::ADMIN_MAIN_PAGE);
                    }
                    $params = [
                        'flag' => 'false',
                        'communicate' => Config::INCORRECT_LOGIN_OR_PASSWORD
                    ];
                }
                $this->renderTemplate('adminLogIn.html', $params);
                break;
            case Routing::ADMIN_MAIN_PAGE:
                $this->checkAccess();
                $params['admin'] = $this->session->get('login');
                $this->renderTemplate('adminMainPage.html', $params);
                break;
            case Routing::ADMIN_ADD_GAME:
                $this->checkAccess();

                $gameName = $this->request->getParamsFormPost('name');
                $author = $this->request->getParamsFormPost('author');
                $dataRelease = $this->request->getParamsFormPost('data_release');
                $price = $this->request->getParamsFormPost('price');
                $availableItems = $this->request->getParamsFormPost('available_items');
                $category = $this->request->getParamsFormPost('id_category');

                if (
                    $gameName &&
                    $author &&
                    $dataRelease &&
                    $price &&
                    $availableItems &&
                    $category
                ) {

                    $this->game->addGame($this->game->createObject($_POST));
                    $users = $this->users->getAll();

                    $message = 'Na portalu http://krolb.ayz.pl/Sklep/httpdocs pojawila sie nowa gra, pt.'.$gameName.', ktora moze Cie zainteresowac';
                    foreach ($users as $user){

                        if($category === $user->getIdCategory()){
                            @mail($user->getUserName(), Config::NEW_GAME_FOR_USER, $message);
                        }
                    }
                    $params['flag'] = 'true';
                    $params['communicate'] = Config::ADDED_GAME;
                }

                $params['categories'] = $this->gameCategory->getAll();

                $this->renderTemplate('adminAddGame.html', $params);
                break;
            case Routing::ADMIN_ADD_CATEGORY:
                $this->checkAccess();
                $params = [];
                if (($category = $this->request->getParamsFormPost('category', false)) !== false) {
                    if ($this->formValidator->formValidate($_POST)) {
                        if ($this->gameCategory->addCategory($category)) {
                            $params = [
                                'flag' => 'true',
                                'communicate' => Config::ADDED_CATEGORY
                            ];
                        } else {
                            $params = [
                                'flag' => 'false',
                                'communicate' => Config::THE_SAME_VALUE
                            ];
                        }
                    } else {
                        $params = [
                            'flag' => 'false',
                            'communicate' => $this->formValidator->getCommunicate()
                        ];
                    }
                }
                if (($idCategory = $this->request->getParamFormGet('removeCategory', false)) !== false) {
                    $this->gameCategory->remove($idCategory);
                }
                $params['categories'] = $this->gameCategory->getAll();

                $this->renderTemplate('adminAddCategory.html', $params);
                break;
            case Routing::ADMIN_CHECK_STORE:
                $this->checkAccess();
                if (($id = $this->request->getParamFormGet('removeGame'))) {
                    $this->game->remove($id);
                }
                if (($photoId = $this->request->getParamsFormPost('photoId')) &&
                    ($file = $this->request->getParamsFromFiles('file'))
                ) {
                    if ($this->imageValidator->validateFile($file, Config::MB * 0.5)) {
                        $name = $this->imageValidator->getName();
                        $this->game->edit($photoId, 'photo', $name);
                        $params['flag'] = 'true';
                        $params['communicate'] = $this->imageValidator->getCommunicate();
                    } else {
                        $params['flag'] = 'false';
                        $params['communicate'] = $this->imageValidator->getCommunicate();
                    }
                }
                $params['games'] = $this->game->getAll();

                $this->renderTemplate('adminStore.html', $params);
                break;
            case Routing::ADMIN_LOG_OUT:
                $this->checkAccess();
                $this->session->kill();
                $this->redirect(Routing::MAIN_PAGE);
                break;
            case Routing::ADMIN_ORDERS:
                $this->checkAccess();
                if(($isExecuted = $this->request->getParamsFormPost('isExecuted'))&&
                    ($orderId = $this->request->getParamsFormPost('orderId'))){
                    $this->orders->edit($orderId, 'is_executed', $isExecuted);
                }
                $params['orders'] = $this->orders->getAll();

                $this->renderTemplate('adminOrders.html', $params);
                break;
            case Routing::GAMES:
                if (($category = $this->request->getParamFormGet('category'))) {
                    $params['games'] = $this->game->getElementsByCategory("'$category'");
                    $this->renderTemplate('games.html', $params);
                } else {
                    $this->redirect('index');
                }


                break;
            case Routing::FINISH:

                $params = [];
                $id = $this->request->getParamsFormPost('id');
                $items = $this->request->getParamsFormPost('available_items');
                $email = $this->request->getParamsFormPost('mail');

                if ($id && $items) {

                    $this->session->put('id', $id);
                    $this->session->put('available_items', $items);

                } elseif (
                    $email &&
                    ($id = $this->session->get('id')) &&
                    ($availableItems = $this->session->get('available_items'))
                ) {
                    $game = $this->game->getElementById($id);
                    $title = $game->getName();
                    $price = $game->getPrice();
                    $category = $game->getCategory();
                    $idCategory = $game->getIdCategory();
                    $items = $game->getAvailableItems() - $availableItems;
                    $this->session->unsetVariable('available_items');
                    $this->session->unsetVariable('id');
                    if ($items < 0) {
                        $params['flag'] = 'false';
                        $params['communicate'] = Config::INCORRECT_COUNT_OF_ITEMS;
                    } elseif ($items >= 0) {
                        $flag = true;
                        if ($items === 0) {
                            $admin = $this->account->getUserByLogin('admin');
                            $mail = $admin->getEmail();
                            $message = 'Klient wlasnie kupil ostatni egzemplarz gry '
                                . $title . ' z kategorii: ' . $category;
                            $flag = @mail($mail, Config::EMPTY_STORAGE, $message);
                        }
                        $message = 'Gratuluje, przed chwila kupiles/as gre pt. '
                            . $title . ' z kategorii: ' . $category . ' za cene: ' . $price;
                        if ($this->formValidator->checkEmail($email)){

                            if(@mail($email, Config::CONGRAT, $message) && $flag){

                                $this->users->setSubscribe($email, $idCategory);
                                $user = $this->users->getElementByMail($email);
                                $this->orders->addOrder($id, $user->getId(), 'do zrealizowania', $availableItems );
                                $this->game->edit($id, 'available_items', $items);


                                $params['flag'] = 'true';
                                $params['communicate'] = Config::CONFIRM;
                            }else{
                                $params['flag'] = 'false';
                                $params['communicate'] = Config::MAIL_FAILED;
                            }

                        } else {
                            $params['flag'] = 'false';
                            $params['communicate'] = $this->formValidator->getCommunicate();
                        }
                    }
                }
                $this->renderTemplate('finish.html', $params);



        break;
    default:
        $params['categories'] = $this->gameCategory->getAll();
        $this->renderTemplate('index.html', $params);
        break;
    }
}

private
function renderTemplate($template, array $params)
{
    try {

        echo $this->templates->render($template, $params);

    } catch (Twig_Error_Loader $e) {
        echo $e->getMessage();
    } catch (Twig_Error_Syntax $e) {
        echo $e->getMessage();
    } catch (Twig_Error_Runtime $e) {
        echo $e->getMessage();
    }
}

private
function checkAccess()
{
    $logged = $this->session->get('logged', false);
    $login = $this->session->get('login', false);
    if ($logged !== 'loggedIn' && $login === false) {
        $this->redirect('index');
    }
}

private
function redirect($routing)
{
    header('Location: ?action=' . $routing);
    exit();
}

}