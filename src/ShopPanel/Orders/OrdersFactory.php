<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 13:30
 */

namespace Bkrol\GameShop\ShopPanel\Orders;


class OrdersFactory
{
    public static function buildCompleteElement($data){
       if(
           array_key_exists('id', $data)&&
           array_key_exists('name', $data)&&
           array_key_exists('category', $data)&&
           array_key_exists('price', $data)&&
           array_key_exists('user_name', $data)&&
           array_key_exists('is_executed', $data)&&
           array_key_exists('order_date', $data)&&
           array_key_exists('count', $data)
       ){
           $builder = new OrdersBuilder();
           return $builder
               ->withName($data['name'])
               ->withCategory($data['category'])
               ->withPrice($data['price'])
               ->withIdOrder($data['id'])
               ->withUserName($data['user_name'])
               ->withIsExecuted($data['is_executed'])
               ->withOrderDate($data['order_date'])
               ->withCount($data['count'])
               ->buildGame();
       }
        return false;
    }


}