<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 11:52
 */

namespace Bkrol\GameShop\ShopPanel\Orders;


use Bkrol\GameShop\ShopPanel\Games\Games;

class Orders extends Games
{
    private $id;
    private $userName;
    private $isExecuted;
    private $orderDate;
    private $count;
    public function __construct($id, $userName,$isExecuted, $orderDate, $count, $idGame, $name, $author, $dataRelease, $price, $photo, $availableItems, $idCategory, $category)
    {
        parent::__construct($idGame, $name, $author, $dataRelease, $price, $photo, $availableItems, $idCategory, $category);
        $this->id = $id;
        $this->userName = $userName;
        $this->isExecuted = $isExecuted;
        $this->orderDate = $orderDate;
        $this->count = $count;
    }

    public function getOrderId(){
        return $this->id;
    }

    public function getUserName(){
        return $this->userName;
    }
    public function getIsExecuted(){
        return $this->isExecuted;
    }
    public function getOrderDate(){
        return $this->orderDate;
    }
    public function getCount(){
        return $this->count;
    }
}