<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 12:03
 */

namespace Bkrol\GameShop\ShopPanel\Orders;




use Bkrol\GameShop\ShopPanel\Games\GamesBuilder;

class OrdersBuilder extends GamesBuilder
{

    private $idOrder ='';
    private $userName='';
    private $count='';
    private $orderDate='';
    private $isExecuted='do zrealizowania';

public function buildGame(){
    return new Orders($this->idOrder, $this->userName, $this->isExecuted,
        $this->orderDate,$this->count, $this->id,$this->name,$this->author, $this->dataRelease,
        $this->price,$this->photo, $this->availableItems,$this->id_category, $this->category);
}
    public function withIdOrder($param){
        $this->idOrder = $param;
        return $this;
    }
    public function withUserName($param){
        $this->userName = $param;
        return $this;
    }
    public function withCount($param){
        $this->count = $param;
        return $this;
    }
    public function withOrderDate($param){
        $this->orderDate = $param;
        return $this;
    }
    public function withIsExecuted($param){
        $this->isExecuted = $param;
        return $this;
    }

}