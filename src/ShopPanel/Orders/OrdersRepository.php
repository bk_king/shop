<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 12:03
 */

namespace Bkrol\GameShop\ShopPanel\Orders;


use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\DbManagement\DbManagement;

class OrdersRepository
{
    public function getAll(){
        $collection = [];
        $accountData = DbManagement::getSelect(
            'SELECT g.id_category, g.name,  g.price, u.user_name, c.category, o.is_executed, o.count, o.order_date, o.id
            FROM games AS g, categories AS c, users AS u, orders AS o
             WHERE g.id_category=c.id AND o.id_users=u.id AND o.id_games=g.id');
var_dump($accountData);
        die();
        if($accountData){
            foreach ($accountData as $value){
                $collection[] = OrdersFactory::buildCompleteElement($value);
            }
        }

        return $collection;

    }

    public function edit($id, $column, $value)
    {

        return DbManagement::updateData(Config::ORDERS, [['column' => $column, 'value' => $value]], [
            ['column' => 'id', 'value' => $id]
        ]);

    }

    public function addOrder($idGame, $idUsers, $isExecuted, $items)
    {
        return DbManagement::insertIntoDb(Config::ORDERS, [
            'id_games' => $idGame,
            'id_users' => $idUsers,
            'is_executed' => $isExecuted,
            'count' => $items
        ]);
    }
}