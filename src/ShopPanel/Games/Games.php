<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 12:05
 */

namespace Bkrol\GameShop\ShopPanel\Games;


use Bkrol\GameShop\ShopPanel\Category\GameCategory;

class Games extends GameCategory
{
    private $id;
    private $name;
    private $author;
    private $dataRelease;
    private $price;
    private $photo;
    private $availableItems;

    public function __construct($id, $name, $author, $dataRelease, $price, $photo, $availableItems, $id_category, $category)
    {
        parent::__construct($id_category, $category);
        $this->id = $id;
        $this->name = $name;
        $this->author = $author;
        $this->dataRelease = $dataRelease;
        $this->price = $price;
        $this->photo = $photo;
        $this->availableItems = $availableItems;


    }

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }

    public function getAuthor()
    {
        return $this->author;
    }


    public function getDataRelease()
    {
        return $this->dataRelease;
    }


    public function getPrice()
    {
        return $this->price;
    }


    public function getPhoto()
    {
        return $this->photo;
    }


    public function getAvailableItems()
    {
        return $this->availableItems;
    }

}