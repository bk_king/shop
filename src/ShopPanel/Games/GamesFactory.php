<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 12:21
 */

namespace Bkrol\GameShop\ShopPanel\Games;


class GamesFactory
{
    public static function buildGame($data)
    {
        if (
            array_key_exists('id', $data) &&
            array_key_exists('name', $data) &&
            array_key_exists('id_category', $data) &&
            array_key_exists('author', $data) &&
            array_key_exists('data_release', $data) &&
            array_key_exists('price', $data) &&
            array_key_exists('photo', $data) &&
            array_key_exists('category', $data) &&
            array_key_exists('available_items', $data)


        ) {
            return new Games(
                $data['id'],
                $data['name'],
                $data['author'],
                $data['data_release'],
                $data['price'],
                $data['photo'],
                $data['available_items'],
                $data['id_category'],
                $data['category']
            );
        }
        return false;
    }

    public static function buildGameWithoutIdPhotoCategory($data)
    {
        if (
            array_key_exists('name', $data) &&
            array_key_exists('id_category', $data) &&
            array_key_exists('author', $data) &&
            array_key_exists('data_release', $data) &&
            array_key_exists('price', $data) &&
            array_key_exists('available_items', $data)
        ) {

            $builder = new GamesBuilder();
            return $builder
                ->withName($data['name'])
                ->withAuthor($data['author'])
                ->withDataRelease($data['data_release'])
                ->withPrice($data['price'])
                ->withAvailableItems($data['available_items'])
                ->withIdCategory($data['id_category'])
                ->build();
        }

    }

    public static function buildGameWithoutIdCategory($data)
    {
        if (
            array_key_exists('id', $data) &&
            array_key_exists('name', $data) &&
            array_key_exists('category', $data) &&
            array_key_exists('author', $data) &&
            array_key_exists('data_release', $data) &&
            array_key_exists('price', $data) &&
            array_key_exists('photo', $data) &&
            array_key_exists('available_items', $data)
        ) {

            $builder = new GamesBuilder();
            return $builder
                ->withId($data['id'])
                ->withName($data['name'])
                ->withAuthor($data['author'])
                ->withDataRelease($data['data_release'])
                ->withPrice($data['price'])
                ->withAvailableItems($data['available_items'])
                ->withCategory($data['category'])
                ->withPhoto($data['photo'])
                ->build();
        }

    }
}