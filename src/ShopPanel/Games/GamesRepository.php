<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 12:25
 */

namespace Bkrol\GameShop\ShopPanel\Games;


use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\DbManagement\DbManagement;

class GamesRepository
{
    public function getAll(){
        $collection = [];
        $accountData = DbManagement::getSelect(
            'SELECT g.id, g.name, g.author, g.data_release, g.price, g.photo, g.available_items, c.category 
            FROM games AS g, categories AS c
             WHERE g.id_category=c.id');

        if($accountData){
            foreach ($accountData as $value){
                $collection[] = GamesFactory::buildGameWithoutIdCategory($value);
            }
        }

        return $collection;

    }
    public function createObject(array $data){
        return GamesFactory::buildGameWithoutIdPhotoCategory($data);
    }

    public function addGame(Games $game)
    {
        $accountData = DbManagement::selectData(Config::GAMES, [
            ['column' => 'name', 'value' => $game->getName(), 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'id_category', 'value' => $game->getIdCategory(), 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'author', 'value' => $game->getAuthor(), 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'data_release', 'value' => $game->getDataRelease(), 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'price', 'value' => $game->getPrice(), 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'available_items', 'value' => $game->getAvailableItems(), 'logic_oper' => '=', 'oper' => '']
        ]);

        if (!$accountData) {
            return DbManagement::insertIntoDb(Config::GAMES, [
                'name' => $game->getName(),
                'id_category' => $game->getIdCategory(),
                'author' => $game->getAuthor(),
                'data_release' => $game->getDataRelease(),
                'price' => $game->getPrice(),
                'available_items' => $game->getAvailableItems()
            ]);
        }
        return false;
    }
    public function edit($id, $column, $value)
    {

        return DbManagement::updateData(Config::GAMES, [['column' => $column, 'value' => $value]], [
            ['column' => 'id', 'value' => $id]
        ]);

    }
    public function remove($id)
    {
        return DbManagement::deleteFromDb(Config::GAMES, [
            ['column' => 'id', 'value' => $id, 'oper' => '=']]);
    }
    public function getElementsByCategory($category){
        $collection = [];
        $accountData = DbManagement::getSelect(
            'SELECT g.id, g.name, g.author, g.data_release, g.price, g.photo, g.available_items, c.category 
            FROM games AS g, categories AS c
             WHERE g.id_category=c.id AND c.category='.$category.' AND g.available_items>0', ['c.category'=>$category]);

        if($accountData){
            foreach ($accountData as $value){
                $collection[] = GamesFactory::buildGameWithoutIdCategory($value);
            }
        }

        return $collection;

    }

    public function getIdCategory($id){
        return DbManagement::getSelect(
            'SELECT  c.id
              FROM games AS g, categories AS c
             WHERE g.id_category=c.id AND g.id='.$id,[],'row');
    }

    public function getElementById($id){
        return GamesFactory::buildGame(
            DbManagement::getSelect(
            'SELECT g.id, g.name, g.id_category, g.author, g.data_release, g.price, g.photo, g.available_items, c.category 
            FROM games AS g, categories AS c
             WHERE g.id_category=c.id AND g.id='.$id,[],'row')
        );
    }
}