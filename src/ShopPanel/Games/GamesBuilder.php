<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 19:48
 */

namespace Bkrol\GameShop\ShopPanel\Games;


class GamesBuilder
{
    protected $id='';
    protected $name='';
    protected $id_category='';
    protected $author='';
    protected $dataRelease='';
    protected $price='';
    protected $photo='';
    protected $availableItems='';
    protected $category='';

    public function build(){
        return new Games(
            $this->id,
            $this->name,
            $this->author,
            $this->dataRelease,
            $this->price,
            $this->photo,
            $this->availableItems,
            $this->id_category,
            $this->category
        );
    }

    public function withCategory($category){
        $this->category = $category;
        return $this;
    }
    public function withId($id){
        $this->id=$id;
        return $this;
    }
    public function withName($name){
        $this->name=$name;
        return $this;
    }
    public function withIdCategory($idCategory){
        $this->id_category=$idCategory;
        return $this;
    }
    public function withAuthor($author){
        $this->author=$author;
        return $this;
    }
    public function withDataRelease($date){
        $this->dataRelease=$date;
        return $this;
    }
    public function withPrice($price){
        $this->price=$price;
        return $this;
    }
    public function withPhoto($photo){
        $this->photo=$photo;
        return $this;
    }
    public function withAvailableItems($items){
        $this->availableItems=$items;
        return $this;
    }

}