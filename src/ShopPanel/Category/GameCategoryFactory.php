<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 11:19
 */

namespace Bkrol\GameShop\ShopPanel\Category;


class GameCategoryFactory
{
    public static function buildCategory($data)
    {
        if (array_key_exists('id', $data) &&
            array_key_exists('category', $data)
        ) {
            return new GameCategory($data['id'], $data['category']);
        }
        return false;
    }

}