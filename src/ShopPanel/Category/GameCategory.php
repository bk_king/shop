<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 11:17
 */

namespace Bkrol\GameShop\ShopPanel\Category;


class GameCategory
{
    protected $id_category;
    protected $category;

    public function __construct($id , $category)
    {
        $this->id_category = $id;
        $this->category = $category;
    }


    public function getIdCategory()
    {
        return $this->id_category;
    }


    public function getCategory()
    {
        return $this->category;
    }


}