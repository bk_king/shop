<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 11:22
 */

namespace Bkrol\GameShop\ShopPanel\Category;


use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\DbManagement\DbManagement;

class GameCategoryRepository
{
    public function getAll()
    {
        $collection = [];
        $accountData = DbManagement::selectData(Config::CATEGORY);

        if ($accountData !== false) {
            foreach ($accountData as $value) {
                $collection[] = GameCategoryFactory::buildCategory($value);
            }
        }


        return $collection;
    }

    public function addCategory($category)
    {
        $accountData = DbManagement::selectData(Config::CATEGORY, [
            ['column' => 'category', 'value' => $category, 'logic_oper' => '=', 'oper' => '']
        ]);

        if (!$accountData) {
            return DbManagement::insertIntoDb(Config::CATEGORY, ['category' => $category]);
        }
        return false;
    }

    public function remove($id)
    {
        return DbManagement::deleteFromDb(Config::CATEGORY, [
            ['column' => 'id', 'value' => $id, 'oper' => '=']]);
    }
}