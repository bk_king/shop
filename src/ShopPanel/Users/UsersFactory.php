<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 09:01
 */

namespace Bkrol\GameShop\ShopPanel\Users;


class UsersFactory
{
    public static function buildCompleteObject($data)
    {
        if (array_key_exists('id', $data) &&
            array_key_exists('user_name', $data) &&
            array_key_exists('id_category', $data) &&
            array_key_exists('category', $data)
        ) {
            $builder =  new UserBuilder();
           return $builder
               ->withId($data['id'])
               ->withUserName($data['user_name'])
               ->withIdCategory($data['id_category'])
               ->withCategory($data['category'])
               ->build();
        }
        return false;
    }

    public static function buildWithIdMailIdCategory($data){
        if (array_key_exists('id', $data) &&
            array_key_exists('user_name', $data) &&
            array_key_exists('id_category', $data)
        ){

            $builder =  new UserBuilder();
            return $builder
                ->withId($data['id'])
                ->withUserName($data['user_name'])
                ->withIdCategory($data['id_category'])
                ->build();
        }
        return false;
    }

}