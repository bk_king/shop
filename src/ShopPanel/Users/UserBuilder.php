<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 08:58
 */

namespace Bkrol\GameShop\ShopPanel\Users;


class UserBuilder
{
    private $id ='';
    private $userName = '';
    private $idCategory = '';
    private $category = '';


    public function build(){
        return new Users($this->id, $this->userName, $this->idCategory, $this->category);
    }

    public function withId($id){
        $this->id = $id;
        return $this;
    }
    public function withUserName($param){
        $this->userName = $param;
        return $this;
    }
    public function withIdCategory($param){
        $this->idCategory = $param;
        return $this;
    }
    public function withCategory($param){
        $this->category = $param;
        return $this;
    }

}