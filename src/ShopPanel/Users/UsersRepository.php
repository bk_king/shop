<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 09:07
 */

namespace Bkrol\GameShop\ShopPanel\Users;


use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\DbManagement\DbManagement;
use Bkrol\GameShop\ShopPanel\Games\Games;

class UsersRepository
{
    public function getAll()
    {
        $collection = [];
        $accountData = DbManagement::getSelect(
            'SELECT u.id, u.id_category, u.user_name, c.category 
            FROM ' . Config::USERS . ' AS u, ' . Config::CATEGORY . ' AS c
             WHERE u.id_category=c.id');

        if ($accountData) {
            foreach ($accountData as $value) {
                $collection[] = UsersFactory::buildCompleteObject($value);
            }
        }

        return $collection;
    }

    public function getElementByMail($mail)
    {
        $accountData = DbManagement::selectData(Config::USERS, [
            ['column' => 'user_name', 'value' => $mail, 'logic_oper' => '=', 'oper' => ''],
        ],['*'], 'row');

        if($accountData){
            return UsersFactory::buildWithIdMailIdCategory($accountData);
        }
        return false;
    }

    public function setSubscribe($mail, $idCategory)
    {

        if (($user = $this->getElementByMail($mail))) {

            return DbManagement::updateData(Config::USERS, [['column' => 'id_category', 'value' => $idCategory]], [
                ['column' => 'id', 'value' => $user->getId()]
            ]);
        }
        return DbManagement::insertIntoDb(Config::USERS, [
            'user_name' => $mail,
            'id_category' => $idCategory
        ]);


    }


}