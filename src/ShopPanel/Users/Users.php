<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.09.2016
 * Time: 08:54
 */

namespace Bkrol\GameShop\ShopPanel\Users;


use Bkrol\GameShop\ShopPanel\Category\GameCategory;

class Users extends GameCategory
{
    private $id;
    private $userName;

    public function __construct($id, $userName, $idCategory, $category)
    {
        parent::__construct($idCategory, $category);
        $this->id = $id;
        $this->userName = $userName;
    }

    public function getId(){
        return $this->id;
    }
    public function getUserName(){
        return $this->userName;
    }

}