<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 19:07
 */

namespace Bkrol\GameShop\DbManagement;


use Bkrol\GameShop\Config\Config;

class DbManagement
{
    private static function getConnect()
    {
        try {
            $conn = new \PDO(Config::DB_NAME_AND_HOST, Config::USER, Config::PASS);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }
        $conn->query("SET NAMES 'utf8'");
        return $conn;


    }

    public static function getSelect($query, array $params = [], $fetch = 'all')
    {
        $conn = self::getConnect();
        $sth = $conn->prepare($query);
        try {
            $sth->execute($params);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }

        if ($fetch === 'all') {
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
        } elseif ($fetch === 'row') {
            $result = $sth->fetch(\PDO::FETCH_ASSOC);
        }
        $conn = null;
        $sth = null;
        return $result;

    }

    public static function selectData($tableName, array $where=[], array $columns = ['*'], $fetch = 'all')
    {

        $conn = self::getConnect();
        $params = [];
        $query = "SELECT ";
        foreach ($columns as $column) {
            $query .= $column . ', ';
        }
        $query = substr($query, 0, strlen($query) - 2);
        $query .= " FROM {$tableName}";
        if (count($where) !== 0) {
            $query .= " WHERE";
            foreach ($where as $val) {
                if (array_key_exists('column', $val) &&
                    array_key_exists('logic_oper', $val) &&
                    array_key_exists('value', $val) &&
                    array_key_exists('oper', $val)
                ) {
                    $params[':' . $val['column']] = $val['value'];
                    $query .= ' ' . $val['column'] . $val['logic_oper'] . ':' . $val['column'] . ' ' . $val['oper'];

                    if (empty($val['oper'])) {
                        break;
                    }
                } else {
                    return false;
                }
            }
        }


        $sth = $conn->prepare($query);
        try {
            $sth->execute($params);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }
        if ($fetch === 'all') {
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
        } elseif ($fetch === 'row') {
            $result = $sth->fetch(\PDO::FETCH_ASSOC);
        }

        $conn = null;
        $sth = null;
        return $result;
    }

    public static function insertIntoDb($tableName, array $columns)
    {
        $conn = DbManagement::getConnect();
        $query = "INSERT INTO {$tableName} (";

        foreach ($columns as $column=>$value) {
            $query .= $column . ', ';
            $params[':'.$column] = $value;
        }
        $query = substr($query, 0, strlen($query) - 2);
        $query .= ') VALUES (';

        foreach ($columns as $column => $value) {
            $query .= ':'.$column . ', ';

        }
        $query = substr($query, 0, strlen($query) - 2);
        $query .= ')';

        $sth = $conn->prepare($query);
        try {
            $sth->execute($params);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }

        $conn = null;
        $sth = null;
        return true;

    }

    public static function deleteFromDb($tableName, array $where)
    {
        if (count($where) === 0) {
            return false;
        }
        $conn = DbManagement::getConnect();
        $params = [];
        $query = "DELETE FROM {$tableName}";

        $query .= " WHERE";
        foreach ($where as $val) {
            if (array_key_exists('column', $val) &&
                array_key_exists('value', $val) &&
                array_key_exists('oper', $val)
            ) {
                $params[':' . $val['column']] = $val['value'];
                $query .= ' ' . $val['column'] . " = " . ':' . $val['column'] . ' ' . $val['oper'];

            } else {
                return false;
            }
        }
        $query = substr($query, 0, strlen($query) - 2);

        $sth = $conn->prepare($query);
        try {
            $sth->execute($params);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }

        $conn = null;
        $sth = null;
        return true;

    }

    public static function updateData($tableName, array $set, array $where)
    {
        $conn = DbManagement::getConnect();
        $query = "UPDATE {$tableName} SET";

        foreach ($set as $val) {
            if (array_key_exists('column', $val) &&
                array_key_exists('value', $val)
            ) {
                $params[':' . $val['column']] = $val['value'];
                $query .= ' ' . $val['column'] . " = " . ':' . $val['column'] . ',';

            } else {
                return false;
            }
        }
        $query = substr($query, 0, strlen($query) - 1);
        $query .= " WHERE ";
        foreach ($where as $val) {
            if (array_key_exists('column', $val) &&
                array_key_exists('value', $val)
            ) {
                $params[':' . $val['column']] = $val['value'];
                $query .= ' ' . $val['column'] . " = " . ':' . $val['column'] . ',';


            } else {
                return false;
            }
        }
        $query = substr($query, 0, strlen($query) - 1);

        $sth = $conn->prepare($query);
        try {
            $sth->execute($params);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }

        $conn = null;
        $sth = null;
        return true;
    }

}