<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 19:01
 */

namespace Bkrol\GameShop\AdminPanel;


class AccountFactory
{
    public static function buildAccount($data)
    {
        if (
        array_key_exists('id', $data) &&
        array_key_exists('login', $data) &&
        array_key_exists('password', $data) &&
        array_key_exists('email', $data)
        ) {
        return new Account($data['id'], $data['login'], $data['password'], $data['email']);
    }

        return false;
    }
}