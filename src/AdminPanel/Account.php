<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 18:57
 */

namespace Bkrol\GameShop\AdminPanel;


class Account
{
    private $id;
    private $login;
    private $password;
    private $email;

    public function __construct($id,$login, $password, $email)
    {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;
    }


    public function getLogin()
    {
        return $this->login;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function getId()
    {
        return $this->id;
    }

}