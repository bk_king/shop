<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 19:05
 */

namespace Bkrol\GameShop\AdminPanel;


use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\DbManagement\DbManagement;

class AccountRepository
{
    public function getUserByLoginAndPassword($login, $password)
    {
        $accountData = DbManagement::selectData(Config::ADMIN_TABLE, [
            ['column' => 'login', 'value' => $login, 'logic_oper' => '=', 'oper' => '']
        ],['*'],'row');

        if ($accountData) {
            $accountData = AccountFactory::buildAccount($accountData);
            $passwordFromDb = $accountData->getPassword();
            if (password_verify($password, $passwordFromDb)) {
                return $accountData;
            }
        }
        return false;
    }

    public function getUserByLogin($login)
    {
        if (($accountData = DbManagement::selectData(Config::ADMIN_TABLE, [
            ['column' => 'login', 'value' => $login, 'logic_oper' => '=', 'oper' => '']],['*'], 'row')
        )
        ){
           return AccountFactory::buildAccount($accountData);
        }
        return false;
    }

    public function createAccount($login, $password, $email){

        $accountData = DbManagement::selectData(Config::ADMIN_TABLE, [
            ['column' => 'login', 'value' => $login, 'logic_oper' => '=', 'oper' => 'OR'],
            ['column' => 'email', 'value' => $email, 'logic_oper' => '=', 'oper' => '']
        ]);

        if(!$accountData){
            return DbManagement::insertIntoDb(Config::ADMIN_TABLE, [
                'login' => $login,
                'password' => password_hash($password, PASSWORD_BCRYPT),
                'email' => $email
            ]);
        }
        return false;
    }


}