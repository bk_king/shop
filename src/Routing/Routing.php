<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 17:49
 */

namespace Bkrol\GameShop\Routing;


class Routing
{
    const MAIN_PAGE = 'index';

    const ADMIN_INDEX = 'logIn';
    const ADMIN_MAIN_PAGE = 'adminMain';
    const ADMIN_LOG_OUT = 'logOut';
    const ADMIN_ADD_CATEGORY = 'addCategory';
    const ADMIN_ADD_GAME = 'addGame';
    const ADMIN_CHECK_STORE = 'store';
    const GAMES = 'games';
    const FINISH = 'finish';
    const ADMIN_ORDERS = 'orders';
}