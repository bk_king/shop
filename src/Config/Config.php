<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 17:31
 */

namespace Bkrol\GameShop\Config;


class Config
{
    const DEBUG = true;
    const ABSOLUTE_TEMPLATE_DIR = //'/home/krolb/domains/krolb.ayz.pl/public_html/Sklep/src/Config/../View/';
        __DIR__ . '/../View/';
    const ABSOLUTE_CACHE_TEMPLATE_DIR = //'/home/krolb/domains/krolb.ayz.pl/public_html/src/Config/../../cache';
        __DIR__ . '/../../cache';

    const IMAGES_DIR = 'images/';
    const FILES_DIR = 'files/';

    /* DB Config */
    const DB_NAME_AND_HOST = //'mysql:host=localhost;dbname=krolb_gameshop';

        'mysql:host=localhost;dbname=gameshop';
    const PASS =//'2Lireni2';
        '';
    const USER = //'krolb_admin';
        'root';
    const ADMIN_TABLE = 'admins';
    const CATEGORY = 'categories';
    const GAMES = 'games';
    const USERS = 'users';
    const ORDERS = 'orders';

    /*Communicates*/
    const INCORRECT_LOGIN_OR_PASSWORD = 'Nie poprawny login lub hasło';
    const FORM_TO_SHORT = 'pole jest za krótkie';
    const FORM_NOT_EMPTY = 'pole nie może być puste';
    const EMAIL_INCORRECT = 'nie poprawny adres e-mail';
    const FORM_ONLY_LETTERS_NUMBERS = 'pole może zawierać tylko znaki alfa-numeryczne';
    const FORM_ONLY_LETTERS = 'pole może zawierać tylko litery';
    const MISSING_REQUIRE_DATA = 'brakuje wymaganych danych';
    const ADDED_CATEGORY = 'dodano kategorię poprawnie';
    const THE_SAME_VALUE = 'nie można dodać tej samej kategorii';
    const REMOVE_CORRECT = 'usunięto poprawnie kategorię';
    const ADDED_GAME = 'dodano grę prawidłowo';
    const NOT_ACTUAL_FILE = 'podjęto próbę dodania obcego pliku';
    const INCORRECT_SIZE = 'plik jest za duży';
    const INCORRECT_TYPE = 'nie poprawny typ pliku';
    const UPLOAD_OK = 'plik poprawnie dodany na serwer';
    const UPLOAD_FAIL = 'nie udało się wysłać pliku na serwer';
    const INCORRECT_COUNT_OF_ITEMS = 'nie poprawna ilość zamówionych sztuk';
    const EMPTY_STORAGE = 'pusty magazyn!';
    const CONGRAT = 'udany zakup';
    const CONFIRM = 'za chwilę na skrzynkę e-mail orzymasz potwierdzenie zakupu';
    const MAIL_FAILED = 'nie udało się wysłać maila z potwierdzeniem';
    const NEW_GAME_FOR_USER = 'nowa gra';


    /*UNITS*/
    const MB = 1048576;



}