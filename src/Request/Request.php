<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.09.2016
 * Time: 17:37
 */

namespace Bkrol\GameShop\Request;


class Request
{
    public function getParamFormGet($param, $default = '')
    {
        if(true === array_key_exists($param, $_GET)){
            return $_GET[$param];
        }

        return $default;
    }

    public function getParamsFormPost($param, $default=''){
        if(true === array_key_exists($param, $_POST)){
            return $_POST[$param];
        }

        return $default;
    }

    public function getParamsFromFiles($param, $default = ''){
        if(true === array_key_exists($param, $_FILES)){
            return $_FILES[$param];
        }

        return $default;
    }

    public function lengthPost(){
        return count($_POST);
    }
}