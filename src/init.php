<?php
use Bkrol\GameShop\Config\Config;
use Bkrol\GameShop\Session\Session;
use Bkrol\GameShop\Request\Request;
use Bkrol\GameShop\Controller\Controller;
use Bkrol\GameShop\AdminPanel\AccountRepository;
require_once(__DIR__ . '/../vendor/autoload.php');

$twig = new Twig_Environment(
    new Twig_Loader_Filesystem(Config::ABSOLUTE_TEMPLATE_DIR),
    array(
        'cache' => (false === Config::DEBUG) ? Config::ABSOLUTE_CACHE_TEMPLATE_DIR : false
    )
);

$app = new Controller(
    new Session(),
    new Request(),
    $twig,
    new AccountRepository()
);
$app->run();