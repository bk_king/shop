<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 10.09.2016
 * Time: 01:43
 */

namespace Bkrol\GameShop\Validators;


use Bkrol\GameShop\Config\Config;

class ImageValidator
{
    private $name;
    private $communicate;


    public function validateFile($data, $size, $kind='image')
    {
        if(!$this->checkActualImage($data)) return false;
        if(!$this->checkSize($data, $size)) return false;
        if($kind==='image'){
            if(!$this->checkImageType($data)) return false;
        }elseif ($kind === 'file'){
            if(!$this->checkFileType($data)) return false;
        }
        return $this->sendImage($data, $kind);
    }

    private function checkActualImage($data)
    {
        if (!is_uploaded_file($data['tmp_name'])) {
            $this->setCommunicate(Config::NOT_ACTUAL_FILE);
            return false;
        }
        return true;
    }

    private function checkSize($data, $size)
    {
        if ($data['size'] > $size) {
            $this->setCommunicate(Config::INCORRECT_SIZE);
            return false;
        }
        return true;
    }

    private function checkImageType($data)
    {
        $imageFileType = pathinfo($data['name'], PATHINFO_EXTENSION);


        if (
            $imageFileType !== 'jpg' &&
            $imageFileType !== 'png' &&
            $imageFileType !== 'jpeg' &&
            $imageFileType !== 'gif'
        ) {
            $this->setCommunicate(Config::INCORRECT_TYPE);
            return false;
        }
        return true;
    }

    private function checkFileType($data)
    {
        $imageFileType = pathinfo($data['name'], PATHINFO_EXTENSION);
        if (
            $imageFileType !== 'docx' &&
            $imageFileType !== 'pdf' &&
            $imageFileType !== 'doc'
        ) {
            $this->setCommunicate(Config::INCORRECT_TYPE);
            return false;
        }
        return true;
    }

    private function sendImage($data, $destination)
    {
        if($destination==='image'){
            $name = Config::IMAGES_DIR . $data['name'];
        }elseif($destination==='file'){
            $name = Config::FILES_DIR . $data['name'];
        }

        if (move_uploaded_file($data['tmp_name'], $name)) {
            $this->setCommunicate(Config::UPLOAD_OK);
            $this->setName($name);
            return true;
        }else{
            $this->setCommunicate(Config::UPLOAD_FAIL);
            return false;
        }
    }

    private function setCommunicate($value)
    {
        $this->communicate = $value;
    }

    public function getCommunicate()
    {
        return $this->communicate;
    }

    private function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

}