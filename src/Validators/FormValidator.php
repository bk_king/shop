<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.09.2016
 * Time: 13:58
 */

namespace Bkrol\GameShop\Validators;


use Bkrol\GameShop\Config\Config;

class FormValidator
{
    private $communicate;


    public function formValidate(array $data){
        foreach ($data as $key => $value){

            if($key  === 'name'){
                if(false === $this->checkName($value, $key)) return false;
            }
            elseif($key  === 'surname'){
                if(false === $this->checkName($value, $key)) return false;
            }
            elseif ($key === 'email'){
                if(false === $this->checkEmail($value)) return false;
            }
            elseif ($key === 'message'){
                if(false === $this->checkMessage($value)) return false;
            }
            elseif ($key === 'category'){
                if(false === $this->checkCategory($value)) return false;
            }
        }
        return true;

    }

    private function checkMessage($data){
        $fieldName ='[wiadomość]: ';
        if(strlen($data)<5){
            $this->setCommunicate($fieldName.Config::FORM_TO_SHORT);
            return false;
        }
    }

    private function checkCategory($data){
        $fieldName='[Kategoria]: ';
        if(empty($data)){
            $this->setCommunicate($fieldName.Config::FORM_NOT_EMPTY);
            return false;
        }
        /*if(!preg_match('/^[a-zA-z]*$/', $data)){
            $this->setCommunicate($fieldName.Config::FORM_ONLY_LETTERS);
            return false;
        }*/
        if(strlen($data)<2){
            $this->setCommunicate($fieldName.Config::FORM_TO_SHORT);
            return false;
        }
    }



    public function checkEmail($data){
        $fieldName = '[email]: ';
        if(!filter_var($data, FILTER_VALIDATE_EMAIL)){
            $this->setCommunicate($fieldName.Config::EMAIL_INCORRECT);
            return false;
        }
        return true;
    }
    public function checkPassword($data){
        $fieldName = '[hasło]: ';
        if (!preg_match('/^[a-zA-Z0-9]*$/', $data)){
            $this->setCommunicate($fieldName.Config::FORM_ONLY_LETTERS_NUMBERS);
            return false;
        }
        if(empty($data)){
            $this->setCommunicate($fieldName.Config::FORM_NOT_EMPTY);

            return false;
        }
        if(strlen($data)<8){
            $this->setCommunicate($fieldName.Config::FORM_TO_SHORT);
            return false;
        }
        return true;
    }
    private function checkName($data, $kind='name'){
        if($kind === 'name'){
            $fieldName = '[imię]: ';
        }elseif ($kind === 'surname'){
            $fieldName = '[nazwisko]: ';
        }

        if (!preg_match('/^[a-zA-Z,ą,ć,ę,ł,ń,ó,ś,ź,ż]*$/', $data)){
            $this->setCommunicate($fieldName.Config::FORM_ONLY_LETTERS);

            return false;
        }
        if(empty($data)){
            $this->setCommunicate($fieldName.Config::FORM_NOT_EMPTY);

            return false;
        }
        if(strlen($data)<3){
            $this->setCommunicate($fieldName.Config::FORM_TO_SHORT);
            return false;
        }
    }

    private function setCommunicate($data){
        $this->communicate = $data;
    }


    public function getCommunicate()
    {
        return $this->communicate;
    }

}