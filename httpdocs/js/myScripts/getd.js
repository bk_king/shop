(function(){

    var editIcon = $('span.glyphicon-pencil');
    editIcon.each(function(){
        $(this).on('click', setIdToModal);
    });

    function setIdToModal(){
        var modalWindow = $('#modal-image form div input[name=photoId]');
        modalWindow.val($(this).data('id'));
    }


}());


